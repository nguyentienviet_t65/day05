<!DOCTYPE html>
<html>
<head>
    <title>Xác nhận thông tin đăng ký</title>
</head>
<body>

<h2>Thông tin đăng ký của bạn:</h2>

<?php
if ($_SERVER["REQUEST_METHOD"] === "POST") {
    echo "Họ và tên: " . htmlspecialchars($_POST["fullName"]) . "<br>";
    
    echo "Giới tính: ";
    if (isset($_POST["gender"])) {
        echo htmlspecialchars($_POST["gender"]) . "<br>";
    } else {
        echo "Không xác định<br>";
    }

    echo "Phân khoa: " . htmlspecialchars($_POST["department"]) . "<br>";

    echo "Ngày sinh: " . htmlspecialchars($_POST["dob"]) . "<br>";

    echo "Địa chỉ: " . htmlspecialchars($_POST["address"]) . "<br>";

    // Hình ảnh
    if ($_FILES["image"]["name"]) {
        echo "Hình ảnh: " . htmlspecialchars($_FILES["image"]["name"]) . "<br>";
    } else {
        echo "Hình ảnh: Không có hình ảnh được chọn.<br>";
    }
}
?>

</body>
</html>

<!DOCTYPE html>
<html>
<head>
    <title>Đăng ký</title>
    <style>
        .error-text {
            color: red;
        }
    </style>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
</head>
<body>

<h2>Form Đăng Ký</h2>

<div id="error-message" class="error-text"></div>

<form id="registration-form">
    <label for="name" class="error-text">Hãy nhập tên.</label>
    <br>
    <label for="dob" class="error-text">Hãy nhập ngày sinh.</label>
    <br>
    Họ và tên*: <input type="text" id="fullName" name="fullName"><br><br>
    Giới tính*:
    <input type="radio" id="male" name="gender" value="male"> Nam
    <input type="radio" id="female" name="gender" value="female"> Nữ
    </label>
    <br><br>
    Phân khoa*:
    <input type="text" id="department" name="department" placeholder="Nhập phân khoa">
    <br><br>
    Ngày sinh*:
    <input type="text" id="dob" name="dob" placeholder="dd/mm/yyyy">
    <br><br>
    Địa chỉ: <input type="text" id="address" name="address"><br><br>

    <!-- Add Image Upload -->
    Hình ảnh*: <input type="file" id="image" name="image"><br><br>

    <button type="button" onclick="validateForm()">Đăng ký</button>
</form>

<script>
    function validateForm() {
        var name = $("#fullName").val();
        var dob = $("#dob").val();
        var department = $("#department").val();

        $("#error-message").html(""); // Clear previous error messages

        if (name === "") {
            $("#error-message").append("Hãy nhập tên.<br>");
        }

        if (department === "") {
            $("#error-message").append("Hãy nhập phân khoa.<br>");
        }

        if (dob !== "") {
            var datePattern = /^(\d{2})\/(\d{2})\/(\d{4})$/;
            if (!datePattern.test(dob)) {
                $("#error-message").append("Hãy nhập ngày sinh đúng định dạng dd/mm/yyyy.<br>");
            }
        }

        // Check if an image is selected
        var image = $("#image").val();
        if (image === "") {
            $("#error-message").append("Hãy chọn hình ảnh.<br>");
        }

        if ($("#error-message").html() === "") {
            // If all validation passed, submit the form to confirm.php
            // You can redirect or submit the form using AJAX based on your requirement.
            // In this example, we're redirecting to confirm.php
            window.location.href = "confirm.php";
        }

        // Scroll to the top of the form to show the error messages
        $('html, body').animate({scrollTop: $("#registration-form").offset().top}, 'slow');
    }
</script>

</body>
</html>